﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include "DeviceItem.h"
#include "DeviceType.h"

#include <QObject>
#include <QVector>

namespace AppCore
{
class APPCORE_EXPORT DeviceGroup : public DeviceItem
{
public:
    DeviceGroup();
    ~DeviceGroup();
    void addChild(std::shared_ptr<DeviceItem> child);
    const std::vector<std::shared_ptr<DeviceItem>>& children() const;
    void removeChild(int index);
    void clear();
    int indexOf(std::shared_ptr<DeviceItem> child) const;
    void sort();

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};

class APPCORE_EXPORT TopDeviceGroup : public DeviceGroup
{
public:
    using DeviceGroup::DeviceGroup;
};

class APPCORE_EXPORT SerialPortGroup
    : public TopDeviceGroup
    , public _DeviceTypeHelper<DeviceType::Type::SerialPort>
{
public:
    inline SerialPortGroup() { setName(QStringLiteral("串口设备")); }
    bool contains(const QString& portName) const;
};

class APPCORE_EXPORT UsbTmcGroup
    : public TopDeviceGroup
    , public _DeviceTypeHelper<DeviceType::Type::USB>
{
public:
    inline UsbTmcGroup() { setName(QStringLiteral("USB设备")); }
    bool contains(uint16_t vid, uint16_t pid, const QString& sn) const;
};

class APPCORE_EXPORT NetworkGroup
    : public TopDeviceGroup
    , public _DeviceTypeHelper<DeviceType::Type::Network>
{
public:
    inline NetworkGroup() { setName(QStringLiteral("网络设备")); }
    bool contains(const QString& address) const;
};
} // namespace AppCore