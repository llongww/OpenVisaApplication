﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "DeviceListConfig.h"
#include "Private/JsonAdl.h"

#include <QCustomUi/QCtmFile.h>

#include <QDir>
#include <QFileInfo>
#include <QStandardPaths>

namespace AppCore
{
struct DeviceListConfig::Impl
{
    std::vector<QString> networks;
    static const QString& configPath()
    {
        static QString path { QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/DeviceListConfig.json" };
        return path;
    }
    NLOHMANN_DEFINE_TYPE_INTRUSIVE_WITH_DEFAULT(Impl, networks)
};

DeviceListConfig& DeviceListConfig::instance()
{
    static DeviceListConfig inst;
    return inst;
}

bool DeviceListConfig::save()
{
    if (QDir dir(QFileInfo(Impl::configPath()).path()); !dir.exists())
    {
        if (!dir.mkpath(dir.path()))
            return false;
    }
    nlohmann::json json = *m_impl;
    return QCtmFile::save(Impl::configPath(), QByteArray::fromStdString(json.dump(4)));
}

bool DeviceListConfig::load()
{
    auto ret = QCtmFile::load(Impl::configPath());
    if (!ret)
        return false;
    try
    {
        auto json = nlohmann::json::parse(*ret);
        *m_impl   = json;
        return true;
    }
    catch (...)
    {
        return false;
    }
}

void DeviceListConfig::addNetwork(const QString& addr) { m_impl->networks.push_back(addr); }

void DeviceListConfig::removeNetwork(const QString& addr) { std::erase(m_impl->networks, addr); }

const std::vector<QString>& DeviceListConfig::networks() const { return m_impl->networks; }

DeviceListConfig::DeviceListConfig() : m_impl(std::make_unique<Impl>()) {}

DeviceListConfig::~DeviceListConfig() {}

} // namespace AppCore
