﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "DeviceItem.h"

namespace AppCore
{
struct DeviceItem::Impl
{
    QString name;
    QString descripiton;
    std::weak_ptr<DeviceGroup> parent;
    QString address;
};

DeviceItem::DeviceItem() : m_impl(std::make_unique<Impl>()) {}

DeviceItem::~DeviceItem() {}

void DeviceItem::setName(const QString& name) { m_impl->name = name; }

const QString& DeviceItem::name() const { return m_impl->name; }

void DeviceItem::setDescription(const QString& descripiton) { m_impl->descripiton = descripiton; }

const QString DeviceItem::description() const { return m_impl->descripiton; }

void DeviceItem::setParent(std::shared_ptr<DeviceGroup> parent) { m_impl->parent = parent; }

std::weak_ptr<DeviceGroup> DeviceItem::parent() const { return m_impl->parent; }

void DeviceItem::setAddress(const QString& address) { m_impl->address = address; }

const QString& DeviceItem::address() const { return m_impl->address; }

} // namespace AppCore