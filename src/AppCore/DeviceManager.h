﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include "AppCoreGlobal.h"

#include <QVector>

#include <memory>

namespace AppCore
{
class TopDeviceGroup;
class DeviceGroup;
class NetworkGroup;
class SerialPortGroup;
class UsbTmcGroup;
class APPCORE_EXPORT DeviceManager
{
public:
    static DeviceManager& instance();
    const QVector<std::shared_ptr<TopDeviceGroup>>& topGroups() const;
    std::shared_ptr<SerialPortGroup> serialPortGroup() const;
    std::shared_ptr<UsbTmcGroup> usbTmcGroup() const;
    std::shared_ptr<NetworkGroup> networkGroup() const;

protected:
    DeviceManager();
    ~DeviceManager();

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};
} // namespace AppCore