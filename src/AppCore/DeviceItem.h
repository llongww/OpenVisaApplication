﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include "AppCoreGlobal.h"

#include <QString>

#include <memory>

namespace AppCore
{
class DeviceGroup;
class APPCORE_EXPORT DeviceItem : public std::enable_shared_from_this<DeviceItem>
{
public:
    DeviceItem();
    virtual ~DeviceItem();

    void setName(const QString& name);
    const QString& name() const;
    void setDescription(const QString& descripiton);
    const QString description() const;
    void setParent(std::shared_ptr<DeviceGroup> parent);
    std::weak_ptr<DeviceGroup> parent() const;
    void setAddress(const QString& address);
    const QString& address() const;

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};
} // namespace AppCore