﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "DeviceManager.h"
#include "Device.h"
#include "DeviceGroup.h"
#include "DeviceListConfig.h"

namespace AppCore
{
struct DeviceManager::Impl
{
    QVector<std::shared_ptr<TopDeviceGroup>> groups;
    std::shared_ptr<SerialPortGroup> serialPortGroup;
    std::shared_ptr<UsbTmcGroup> usbTmcGroup;
    std::shared_ptr<NetworkGroup> networkGroup;
    inline Impl()
    {
        groups.push_back(serialPortGroup = std::make_shared<SerialPortGroup>());
        groups.push_back(usbTmcGroup = std::make_shared<UsbTmcGroup>());
        groups.push_back(networkGroup = std::make_shared<NetworkGroup>());
        DeviceListConfig::instance().load();
        for (const auto& name : DeviceListConfig::instance().networks())
        {
            networkGroup->addChild(std::make_shared<NetworkDevice>(name));
        }
    }
};

DeviceManager& DeviceManager::instance()
{
    static DeviceManager inst;
    return inst;
}

const QVector<std::shared_ptr<TopDeviceGroup>>& DeviceManager::topGroups() const { return m_impl->groups; }

std::shared_ptr<SerialPortGroup> DeviceManager::serialPortGroup() const { return m_impl->serialPortGroup; }

std::shared_ptr<UsbTmcGroup> DeviceManager::usbTmcGroup() const { return m_impl->usbTmcGroup; }

std::shared_ptr<NetworkGroup> DeviceManager::networkGroup() const { return m_impl->networkGroup; }

DeviceManager::DeviceManager() : m_impl(std::make_unique<Impl>()) {}

DeviceManager::~DeviceManager() {}

} // namespace AppCore