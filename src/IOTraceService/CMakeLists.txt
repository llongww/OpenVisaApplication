﻿set(TARGET_NAME IOTraceService)

set(CMAKE_AUTOMOC ON)

add_compile_definitions(IOTRACESERVICE_LIB)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core Network REQUIRED)
find_package(OpenVisa CONFIG REQUIRED)
find_package(QCustomUi CONFIG REQUIRED)

file(GLOB SOURCES "*.cpp")
file(GLOB HEADERS "*.h")

source_group("Headers" FILES ${HEADERS})
source_group("Sources" FILES ${SOURCES})

if(MSVC)
    set(VERSIONINFO_RC ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}.rc)
    configure_file("${PROJECT_SOURCE_DIR}/cmake/LibraryVersion.rc.in" "${VERSIONINFO_RC}")
    list(APPEND RESOURCES ${VERSIONINFO_RC})
endif(MSVC)

add_library(${TARGET_NAME} SHARED ${SOURCES} ${HEADERS} ${RESOURCES})

target_link_libraries(${TARGET_NAME} PRIVATE Qt::Core Qt::Network QCustomUi OpenVisa)

install(TARGETS ${TARGET_NAME} RUNTIME DESTINATION ${INSTALL_BINDIR}/${CMAKE_BUILD_TYPE})
