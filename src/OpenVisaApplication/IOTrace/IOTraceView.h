﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include "ui_IOTraceView.h"

#include <QCustomUi/QCtmWindow.h>

class IOTraceView : public QCtmWindow
{
    Q_OBJECT
public:
    IOTraceView(QWidget* parent = nullptr);
    ~IOTraceView();
private slots:
    void onStartAction();
    void onStopAction();
    void onSettingsAction();

protected:
    void closeEvent(QCloseEvent* event) override;

private:
    Ui::IOTraceView ui;
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};
