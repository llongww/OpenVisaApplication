﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "IconProxyStyle.h"

QPixmap IconProxyStyle::generatedIconPixmap(QIcon::Mode iconMode, const QPixmap& pixmap, const QStyleOption* opt) const
{
    if (iconMode == QIcon::Mode::Disabled)
    {
        QImage im = pixmap.toImage().convertToFormat(QImage::Format_ARGB32);
        for (int y = 0; y < im.height(); ++y)
        {
            QRgb* scanLine = reinterpret_cast<QRgb*>(im.scanLine(y));
            for (int x = 0; x < im.width(); ++x)
            {
                QRgb pixel = *scanLine;
                int ci     = (qRed(pixel) + qGreen(pixel) + qBlue(pixel)) / 3 / 3;
                *scanLine  = QColor(ci, ci, ci, qAlpha(pixel)).rgba();
                ++scanLine;
            }
        }
        return QPixmap::fromImage(std::move(im));
    }
    return QProxyStyle::generatedIconPixmap(iconMode, pixmap, opt);
}
