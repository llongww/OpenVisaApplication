﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include <QAbstractItemModel>

namespace AppCore
{
class DeviceItem;
}

class DeviceListModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    DeviceListModel(QObject* parent);
    ~DeviceListModel();

    QModelIndex index(int row, int column, const QModelIndex& parent /* = QModelIndex() */) const override;
    QModelIndex parent(const QModelIndex& index) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    bool hasChildren(const QModelIndex& parent = QModelIndex()) const override;
    std::shared_ptr<AppCore::DeviceItem> deviceItem(const QModelIndex& index) const;
signals:
    void deviceRemoved(std::shared_ptr<AppCore::DeviceItem> item);
public slots:
    void updateSerialPort();
    void updateUsbTmc();
    void addNetworkDevice(const QString& address);
    void removeNetworkDevice(const QModelIndex& index);

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};