﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include "ui_UpdateDialog.h"

#include <QCustomUi/QCtmDialog.h>

class QNetworkReply;
class UpdateDialog : public QCtmDialog
{
    Q_OBJECT
public:
    explicit UpdateDialog(QWidget* parent);
    ~UpdateDialog();
    void setDatas(const QString& version, const QString& description, const QString& downloadUrl);
private slots:
    void onUpdateBtn();
    void onFinished();

private:
    Ui::UpdateDialog ui;
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};