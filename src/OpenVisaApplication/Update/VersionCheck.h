﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include <QObject>

#include <memory>

class QNetworkReply;
class VersionCheck : public QObject
{
    Q_OBJECT
public:
    static VersionCheck& instance();
    void checkUpdate(bool manual);
    const QString& version() const;
    const QString& description() const;
    const QString& downloadUrl() const;
signals:
    void finished(bool manual, bool newVersion);
    void errorOccurred(bool manual, const QString& error);

protected:
    VersionCheck();
    ~VersionCheck();
    void init();
private slots:
    void onNetworkFinished(QNetworkReply* reply);

private:
    void onVersionCheck(bool manual, const QByteArray& data);

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};