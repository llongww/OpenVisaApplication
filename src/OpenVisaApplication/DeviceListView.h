﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include <QWidget>

namespace AppCore
{
class DeviceItem;
}

class DeviceListView : public QWidget
{
    Q_OBJECT
public:
    explicit DeviceListView(QWidget* parent);
    ~DeviceListView();

signals:
    void openDevice(std::shared_ptr<AppCore::DeviceItem> item);
    void onlyShowView(std::shared_ptr<AppCore::DeviceItem> item);
    void closeDevice(std::shared_ptr<AppCore::DeviceItem> item);
private slots:
    void onDoubleClicked(const QModelIndex& index);
    void onOnlyShowViewAction(const QModelIndex& index);
    void onCustomContextMenuRequested(const QPoint& pos);
    void onAddNetworkDevice();
    void onRemoveNetworkDevice(const QModelIndex& index);
    void onUpdateSerialPortDevices();
    void onUpdateUSBs();
    void onDeviceAttributeAction(const QModelIndex& index);
    void onCopyAddressAction();

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};