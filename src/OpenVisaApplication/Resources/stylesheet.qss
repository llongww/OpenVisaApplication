﻿.QCtmTitleBar
{
    background: #3C3C3C;
    color: #848484;
    font-weight: bold;
}

.QCtmTitleBar QMenuBar
{
    background: #3C3C3C;
    color: #D4D4D4;
}

.QCtmTitleBar QMenuBar::item
{
    background: #3C3C3C;
}

.QCtmTitleBar QMenuBar::item:selected
{
    background: #6E6E6E;
}

QCtmTitleBar QAbstractButton:hover
{
    background: #565656;
}

QCtmTitleBar QAbstractButton:pressed
{
    background: #6E6E6E;
}

QCtmTitleBar #closeBtn:hover
{
    background: #C32B1C;
}

QCtmTitleBar #closeBtn:pressed
{
    background: #A2222D;
}

QCtmTitleBar #closeBtn
{
    color: #D4D4D4;
    qproperty-icon: url(:/OpenVisaApplication/Resources/Close.svg);
}

QCtmMaximumSizeButton
{
    qproperty-maximumSizedIcon: url(:/OpenVisaApplication/Resources/Restore.svg);
    qproperty-revertIcon: url(:/OpenVisaApplication/Resources/MaximumSized.svg);
}

QCtmTitleBar #minimumSizeBtn
{
    qproperty-icon: url(:/OpenVisaApplication/Resources/Minimized.svg);
}

.MainWindow
{
    background: #252526;
}

QTreeView
{
    outline: none;
    background: #252526;
    color: #D4D4D4;
    border: none;
}

QTreeView::item:hover
{
    background: #2A2D2E;
}

QTreeView::item:selected
{
    color: white;
    background: #009789;
}


QTreeView::branch:has-children:!has-siblings:closed,
QTreeView::branch:closed:has-children:has-siblings
{
    border-image: none;
    image: url(:/OpenVisaApplication/Resources/RightArrow.svg);
    padding: 5px;
}

QTreeView::branch:open:has-children:!has-siblings,
QTreeView::branch:open:has-children:has-siblings
{
    border-image: none;
    image: url(:/OpenVisaApplication/Resources/DownArrow.svg);
    padding: 5px;
}

QTabWidget, QStackedWidget
{
    background: #1E1E1E;
}

QTabWidget::pane
{
    background: transparent;
}

QTabBar::tab
{
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 15px;
    padding-right: 15px;
    color: #D4D4D4;
    background: #2D2D2D;
}

QTabBar::tab:selected
{
    background: #1E1E1E;
    border-top: 1px solid #FFD3B6;
}

QTabBar::close-button
{
    image: url(:/OpenVisaApplication/Resources/Close.svg);
}

QTabBar::close-button:hover
{
    background: #3B3C3C;
}

.QSplitter::handle:horizontal
{
    width: 2px;
    background: #2e2e2e;
}

QSplitter::handle
{
    background: transparent;
}

QCtmDialog, QDialog, QCtmWindow
{
    background: #252526;
}

QLabel
{
    color: #D4D4D4;
}

QPushButton
{
    background: #3C3C3C;
    border: none;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 15px;
    padding-right: 15px;
    color: #D4D4D4;
}

QPushButton:hover
{
    background: #5C5C5C;
}

TestView
{
    background: #1E1E1E;
}

QCtmTableView
{
    qproperty-showGrid: false;
    qproperty-alternatingRowColors: true;
    alternate-background-color: #262626;
    background: #1E1E1E;
    border: none;
    color: #D4D4D4;
    gridline-color: transparent;
    selection-background-color: #009789;
}

QCtmTableView QTableCornerButton::section
{
    border: none;
    background: #2E2E2E;
}

QCtmTableView QLineEdit
{
    border: none;
    background: #3C3C3C;
    color: #FFD3B6;
}

QCtmTableView::item:selected
{
    color: white;
}

QHeaderView::section
{
    padding: 5px;
    color: #D4D4D4;
    border: none;
    background: #2E2E2E;
}

#vHeader
{
    color: #D4D4D4;
    background: #1E1E1E;
    qproperty-alternatingRowColors: true;
    alternate-background-color: #262626;
}

#vHeader::section,
#vHeader::section:hover,
#vHeader::section:selected,
#vHeader::section:checked
{
    background: transparent;
}

QMenu
{
    color: #D4D4D4;
    background: #252526;
}

QMenu::item:selected
{
    background: #0078D4;
    color: white;
}

QToolButton
{
    padding: 3px;
    border: none;
}

QToolButton:hover
{
    background: #565656;
}

QToolButton:checked
{
    background: #3a4161;
}

QToolButton:disabled
{
    color: palette(dark);
}

QScrollBar:horizontal
{
    border: none;
    background: #2E2E2E;
    height: 15px;
    margin: 0px 15px 0 15px;
}

QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal
{
    background: none;
}

QScrollBar::add-line:horizontal
{
    border: none;
    background: #2E2E2E;
    width: 15px;
    subcontrol-position: right;
    subcontrol-origin: margin;
}

QScrollBar::sub-line:horizontal
{
    border: none;
    background: #2E2E2E;
    width: 15px;
    subcontrol-position: left;
    subcontrol-origin: margin;
}

QScrollBar::handle:horizontal
{
    margin-top: 3px;
    margin-bottom: 3px;
    background: #4D4D4D;
    min-width: 100px;
}

QScrollBar::handle:horizontal:hover, QScrollBar::handle:horizontal:pressed
{
    background: #999999;
}

QScrollBar::right-arrow:horizontal
{
    width: 15px;
    height: 15px;
    image: url(:/OpenVisaApplication/Resources/Right.svg);
}

QScrollBar::left-arrow:horizontal
{
    width: 15px;
    height: 15px;
    image: url(:/OpenVisaApplication/Resources/Left.svg);
}

QScrollBar::right-arrow:horizontal:hover
{
    image: url(:/OpenVisaApplication/Resources/RightHover.svg);
}

QScrollBar::left-arrow:horizontal:hover
{
    image: url(:/OpenVisaApplication/Resources/LeftHover.svg);
}

QScrollBar:vertical
{
    border: none;
    background: #2E2E2E;
    width: 15px;
    margin: 15px 0px 15px 0px;
}

QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical
{
    background: none;
}

QScrollBar::add-line:vertical
{
    border: none;
    background: #2E2E2E;
    height: 15px;
    subcontrol-position: bottom;
    subcontrol-origin: margin;
}

QScrollBar::sub-line:vertical
{
    border: none;
    background: #2E2E2E;
    height: 15px;
    subcontrol-position: top;
    subcontrol-origin: margin;
}

QScrollBar::handle:vertical
{
    margin-left: 3px;
    margin-right: 3px;
    background: #4D4D4D;
    min-height: 100px;
}

QScrollBar::handle:vertical:hover, QScrollBar::handle:vertical:pressed
{
    background: #999999;
}

QScrollBar::down-arrow:vertical
{
    width: 15px;
    height: 15px;
    image: url(:/OpenVisaApplication/Resources/Down.svg);
}

QScrollBar::up-arrow:vertical
{
    width: 15px;
    height: 15px;
    image: url(:/OpenVisaApplication/Resources/Up.svg);
}

QScrollBar::down-arrow:vertical:hover
{
    image: url(:/OpenVisaApplication/Resources/DownHover.svg);
}

QScrollBar::up-arrow:vertical:hover
{
    image: url(:/OpenVisaApplication/Resources/UpHover.svg);
}

QAbstractSpinBox
{
    border: none;
    background: #333333;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 2px;
    color: #FFD3B6;
}

QAbstractSpinBox::up-button, QAbstractSpinBox::down-button
{
    border: none;
    background: transparent;
}

QAbstractSpinBox::up-arrow
{
    image: url(:/OpenVisaApplication/Resources/Up.svg);
}

QAbstractSpinBox::down-arrow
{
    image: url(:/OpenVisaApplication/Resources/Down.svg);
}

QAbstractSpinBox::up-arrow:hover
{
    image: url(:/OpenVisaApplication/Resources/UpHover.svg);
}

QAbstractSpinBox::down-arrow:hover
{
    image: url(:/OpenVisaApplication/Resources/DownHover.svg);
}
