﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/

#include "AddNetworkDeviceDialog.h"

#include <AppCore/DeviceGroup.h>
#include <AppCore/DeviceManager.h>
#include <QCustomUi/QCtmMessageBox.h>

#include <utility>

AddNetworkDeviceDialog::AddNetworkDeviceDialog(QWidget* parent /*= nullptr*/)
{
    ui.setupUi(this->centralWidget());
    connect(ui.okBtn, &QPushButton::clicked, this, &AddNetworkDeviceDialog::onOkBtn);
    connect(ui.cancelBtn, &QPushButton::clicked, this, &QCtmDialog::reject);
    connect(ui.hislipBtn, &QRadioButton::toggled, this, [this](bool checked) { ui.hislipBase->setEnabled(checked); });
    connect(ui.vxi11Btn, &QRadioButton::toggled, this, [this](bool checked) { ui.vxi11Base->setEnabled(checked); });
    connect(ui.rawSocketBtn, &QRadioButton::toggled, this, [this](bool checked) { ui.rawSocketBase->setEnabled(checked); });
}

AddNetworkDeviceDialog::~AddNetworkDeviceDialog() {}

QString AddNetworkDeviceDialog::address() const
{
    if (ui.rawSocketBtn->isChecked())
    {
        return QString("TCPIP::%1::%2::SOCKET").arg(ui.rawSocketIP->ipAddress()).arg(ui.rawSocketPort->value());
    }
    else if (ui.vxi11Btn->isChecked())
    {
        return QString("TCPIP::%1::INSTR").arg(ui.vxi11IP->ipAddress());
    }
    else if (ui.hislipBtn->isChecked())
    {
        return QString("TCPIP::%1::HISLIP").arg(ui.hislipIP->ipAddress());
    }
    std::unreachable();
}

void AddNetworkDeviceDialog::onOkBtn()
{
    auto addr = this->address();
    if (AppCore::DeviceManager::instance().networkGroup()->contains(addr))
    {
        QCtmMessageBox::warning(this, u8"警告", u8"该设备已在列表中");
        return;
    }
    accept();
}
