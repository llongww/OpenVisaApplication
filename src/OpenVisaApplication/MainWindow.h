﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include "ui_MainWindow.h"

#include <QCustomUi/QCtmWindow.h>

namespace OpenVisa
{
class Object;
}

namespace AppCore
{
class DeviceItem;
}

class MainWindow : public QCtmWindow
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow();

private slots:
    void onOpenDevice(std::shared_ptr<AppCore::DeviceItem> item);
    void onOnlyShowView(std::shared_ptr<AppCore::DeviceItem> item);
    void onCloseDevice(std::shared_ptr<AppCore::DeviceItem> item);
    void onTabCloseRequested(int index);
    void onUpdateAction();
    void onNewVersionArrived(bool manual, bool newVersion);

protected:
    void closeEvent(QCloseEvent* event) override;

private:
    void init();
    void openView(std::shared_ptr<AppCore::DeviceItem> item);

private:
    Ui::MainWindow ui;
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};
