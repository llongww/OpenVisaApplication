﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "Excel.h"
#include "../TestView/TestModel.h"

#include <xlnt/xlnt.hpp>

#include <QDir>
#include <QFileInfo>

#include <ranges>

bool Excel::save(TestModel* model, const QString& filepath)
{
    if (QDir dir(QFileInfo(filepath).path()); !dir.exists())
    {
        if (!dir.mkpath(dir.path()))
            return false;
    }
    try
    {
        xlnt::workbook book;
        auto defaultSheet = book.active_sheet();
        for (auto i : std::views::iota(0, model->rowCount()))
        {
            auto cmdCell = defaultSheet.cell(static_cast<int>(TestModel::Columns::Command) + 1, i + 1);
            cmdCell.value(model->data(model->index(i, static_cast<int>(TestModel::Columns::Command))).toString().toStdString());
            auto retCell = defaultSheet.cell(static_cast<int>(TestModel::Columns::Result) + 1, i + 1);
            retCell.value(model->data(model->index(i, static_cast<int>(TestModel::Columns::Result))).toString().toStdString());
            auto commentCell = defaultSheet.cell(static_cast<int>(TestModel::Columns::Comment) + 1, i + 1);
            commentCell.value(model->data(model->index(i, static_cast<int>(TestModel::Columns::Comment))).toString().toStdString());
        }
        book.save(filepath.toStdString());
        return true;
    }
    catch (const std::exception&)
    {
        return false;
    }
}

bool Excel::load(TestModel* model, const QString& filepath)
{
    QFileInfo info(filepath);
    if (!info.exists())
        return false;
    try
    {
        xlnt::workbook book;
        book.load(filepath.toStdString());
        std::map<unsigned int, std::tuple<QString, QString, QString>> datas;
        auto defaultSheet = book.active_sheet();
        int i             = 0;
        for (auto row : defaultSheet.rows(false))
        {
            std::tuple<QString, QString, QString> d;
            if (row.length() >= 1)
                std::get<0>(d) = QString::fromStdString(row[0].to_string());
            if (row.length() >= 2)
                std::get<1>(d) = QString::fromStdString(row[1].to_string());
            if (row.length() >= 3)
                std::get<2>(d) = QString::fromStdString(row[2].to_string());
            datas[i++] = d;
        }
        model->setDatas(datas);
        return true;
    }
    catch (const std::exception&)
    {
        return false;
    }
}
