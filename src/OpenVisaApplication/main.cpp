﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "IconProxyStyle.h"
#include "MainWindow.h"
#include "Update/VersionCheck.h"

#include <QCustomUi/QCtmApplication.h>
#include <QCustomUi/QCtmFile.h>
#include <QCustomUi/QCtmStyleSheet.h>

int main(int argc, char* argv[])
{
    QCtmApplication::setGetObjectDisabled(true);
    QCtmApplication app(argc, argv, QApplication::ApplicationFlags, false);
    app.setWindowIcon(QIcon(":/OpenVisaApplication/Resources/ICON.ico"));
    app.setStyleSheet(QCtmStyleSheet::defaultStyleSheet() + QString(*QCtmFile::load(":/OpenVisaApplication/Resources/stylesheet.qss")));
    app.setStyle(new IconProxyStyle);
    MainWindow w;
    w.showMaximized();
    VersionCheck::instance().checkUpdate(false);
    return app.exec();
}