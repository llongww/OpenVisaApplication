﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "AboutQCustomUiDialog.h"

#include <QCustomUi/Version.h>

AboutQCustomUiDialog::AboutQCustomUiDialog(QWidget* parent /*= nullptr*/) : QCtmDialog(parent)
{
    ui.setupUi(centralWidget());
    ui.version->setText(QString("%1.%2.%3").arg(QCUSTOMUI_VERSION_MAJOR).arg(QCUSTOMUI_VERSION_MINOR).arg(QCUSTOMUI_VERSION_MICRO));
}

AboutQCustomUiDialog::~AboutQCustomUiDialog() {}
