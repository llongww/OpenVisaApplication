﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include "ui_TestView.h"

#include <OpenVisa/Object.h>

namespace AppCore
{
class Device;
}
class TestView : public QWidget
{
    Q_OBJECT
public:
    explicit TestView(std::shared_ptr<AppCore::Device> item, QWidget* parent);
    ~TestView();
    std::shared_ptr<AppCore::Device> device() const;
public slots:
    void onStartAction();
    void onReceiveAction();
    void onQueryAction();
    void onSaveCache();
    void onClearResultAction();
    void onExportAction();
    void onImportAction();

private:
    QString cachePath() const;
    void send(const std::string& cmd);
    void query(const std::string& cmd);
    std::optional<std::string> currentCommand();
    void initToolBar();

private:
    Ui::TestView ui;
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};