﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#pragma once

#include <QAbstractTableModel>

class TestModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum class Columns
    {
        Command,
        Result,
        Comment
    };
    constexpr static auto MaxRows = 1024;
    explicit TestModel(QObject* parent);
    ~TestModel();

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent /* = QModelIndex() */) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role /* = Qt::EditRole */) override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const override;
    void setDatas(const std::map<unsigned int, std::tuple<QString, QString, QString>>& datas, int rowCount = MaxRows);
    Qt::ItemFlags flags(const QModelIndex& index) const override;
public slots:
    void clearResults();

private:
    struct Impl;
    std::unique_ptr<Impl> m_impl;
};