﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "TermCharsDelegate.h"
#include "HexLineEdit.h"

#include <QCustomUi/QCtmComboBox.h>

TermCharsDelegate::TermCharsDelegate(QTableView* parent) : QCtmTableItemDelegate(parent) {}

TermCharsDelegate::~TermCharsDelegate() {}

QWidget* TermCharsDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() != 1)
        return nullptr;
    auto editor = new QCtmComboBox(parent);
    editor->setLineEdit(new HexLineEdit);
    editor->addItems({ "0A", "0D", "0D 0A" });
    return editor;
}

void TermCharsDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    box->setEditText(index.data().toByteArray().toHex(' ').toUpper());
}

void TermCharsDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    auto box       = qobject_cast<QCtmComboBox*>(editor);
    auto termChars = QByteArray::fromHex(box->currentText().toLatin1());
    model->setData(index, termChars);
}

void TermCharsDelegate::initStyleOption(QStyleOptionViewItem* option, const QModelIndex& index) const
{
    QCtmTableItemDelegate::initStyleOption(option, index);
    if (index.column() == 1)
    {
        option->text = index.data().toByteArray().toHex(' ').toUpper();
    }
}
