﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "TimeoutDelegate.h"

#include <QCustomUi/QCtmLongLongSpinBox.h>

TimeoutDelegate::TimeoutDelegate(QTableView* parent) : QCtmTableItemDelegate(parent) {}

TimeoutDelegate::~TimeoutDelegate() {}

QWidget* TimeoutDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() != 1)
        return nullptr;
    auto editor = new QCtmLongLongSpinBox(parent);
    editor->setRange(0, std::numeric_limits<qlonglong>::max());
    editor->setSuffix("ms");
    return editor;
}

void TimeoutDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmLongLongSpinBox*>(editor);
    box->setValue(index.data().toLongLong());
}

void TimeoutDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmLongLongSpinBox*>(editor);
    model->setData(index, box->value());
}

void TimeoutDelegate::initStyleOption(QStyleOptionViewItem* option, const QModelIndex& index) const
{
    QCtmTableItemDelegate::initStyleOption(option, index);
    if (index.column() == 1)
    {
        option->text = index.data().toString() + "ms";
    }
}
