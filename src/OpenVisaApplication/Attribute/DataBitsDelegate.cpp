﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "DataBitsDelegate.h"

#include <QCustomUi/QCtmComboBox.h>

#include <limits>

DataBitsDelegate::DataBitsDelegate(QTableView* parent) : QCtmTableItemDelegate(parent) {}

DataBitsDelegate::~DataBitsDelegate() {}

QWidget* DataBitsDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() != 1)
        return nullptr;
    auto editor = new QCtmComboBox(parent);
    editor->addItems({ "5", "6", "7", "8" });
    return editor;
}

void DataBitsDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    box->setCurrentText(index.data().toString());
}

void DataBitsDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    model->setData(index, box->currentText());
}
