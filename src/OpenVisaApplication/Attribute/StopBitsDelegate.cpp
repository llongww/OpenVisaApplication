﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "StopBitsDelegate.h"

#include <OpenVisa/Attribute.h>
#include <QCustomUi/QCtmComboBox.h>

#include <limits>

StopBitsDelegate::StopBitsDelegate(QTableView* parent) : QCtmTableItemDelegate(parent) {}

StopBitsDelegate::~StopBitsDelegate() {}

QWidget* StopBitsDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() != 1)
        return nullptr;
    auto editor = new QCtmComboBox(parent);
    editor->addItems({ "1", "1.5", "2" });
    return editor;
}

void StopBitsDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    box->setCurrentIndex(index.data().toInt());
}

void StopBitsDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    model->setData(index, box->currentIndex());
}

void StopBitsDelegate::initStyleOption(QStyleOptionViewItem* option, const QModelIndex& index) const
{
    QCtmTableItemDelegate::initStyleOption(option, index);
    if (index.column() == 1)
    {
        switch (static_cast<OpenVisa::StopBits>(index.data().toInt()))
        {
        case OpenVisa::StopBits::One:
            option->text = "1";
            break;
        case OpenVisa::StopBits::OnePointFive:
            option->text = "1.5";
            break;
        case OpenVisa::StopBits::Two:
            option->text = "2";
            break;
        }
    }
}
