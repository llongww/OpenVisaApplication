﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "FlowControlDelegate.h"

#include <QCustomUi/QCtmComboBox.h>

#include <limits>
#include <ranges>

static constexpr auto Items = { u8"无", u8"Software flow control (XON/XOFF)", u8"Hardware flow control (RTS/CTS)" };
static constexpr auto Enums = std::views::enumerate(Items);

FlowControlDelegate::FlowControlDelegate(QTableView* parent) : QCtmTableItemDelegate(parent) {}

FlowControlDelegate::~FlowControlDelegate() {}

QWidget* FlowControlDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() != 1)
        return nullptr;
    auto editor = new QCtmComboBox(parent);
    editor->addItems(Items | std::views::transform([](auto& v) { return QString(v); }) | std::ranges::to<QList>());
    return editor;
}

void FlowControlDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    box->setCurrentIndex(index.data().toInt());
}

void FlowControlDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    model->setData(index, box->currentIndex());
}

void FlowControlDelegate::initStyleOption(QStyleOptionViewItem* option, const QModelIndex& index) const
{
    QCtmTableItemDelegate::initStyleOption(option, index);
    if (index.column() == 1)
    {
        auto i = index.data().toInt();
        if (i >= 0 && i < Enums.size())
        {
            option->text = std::get<1>(Enums[i]);
        }
    }
}
