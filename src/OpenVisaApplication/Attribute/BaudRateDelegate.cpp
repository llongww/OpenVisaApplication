﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "BaudRateDelegate.h"

#include <QCustomUi/QCtmComboBox.h>

#include <limits>

BaudRateDelegate::BaudRateDelegate(QTableView* parent) : QCtmTableItemDelegate(parent) {}

BaudRateDelegate::~BaudRateDelegate() {}

QWidget* BaudRateDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() != 1)
        return nullptr;
    auto editor = new QCtmComboBox(parent);
    editor->setEditable(true);
    editor->setValidator(new QIntValidator(0, std::numeric_limits<int>::max()));
    editor->addItems({ "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200" });
    return editor;
}

void BaudRateDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    box->setEditText(index.data().toString());
}

void BaudRateDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    auto box = qobject_cast<QCtmComboBox*>(editor);
    model->setData(index, box->currentText());
}
