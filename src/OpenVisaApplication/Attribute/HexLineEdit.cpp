﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "HexLineEdit.h"

#include <tao/pegtl.hpp>

namespace PEG
{
struct Context
{
    std::string output;
    std::string byteGroup;
    inline void pushByte()
    {
        if (!output.empty())
            output.push_back(' ');
        output.append_range(byteGroup);
        byteGroup.clear();
    }
    inline bool format()
    {
        if (!byteGroup.empty())
        {
            if (!output.empty())
                output.push_back(' ');
            output.push_back(byteGroup.front());
            return true;
        }
        return false;
    }
    inline void fixup()
    {
        if (!byteGroup.empty()) // 单字符
        {
            output.push_back(' ');
            output.push_back('0');
            output.push_back(byteGroup.front());
        }
    }
};
using namespace tao::pegtl;
using Char       = ascii::xdigit;
using Expression = star<sor<Char, ascii::space, ascii::any>>;

template<typename T>
struct Action
{
};

template<>
struct Action<Char>
{
    template<typename ParseInput>
    static void apply(const ParseInput& in, Context& ctx)
    {
        ctx.byteGroup.push_back(*in.begin());
        if (ctx.byteGroup.size() == 2)
        {
            ctx.pushByte();
        }
    }
};
} // namespace PEG

QValidator::State HexValidator::validate(QString& input, int& pos) const
{
    PEG::Context ctx;
    auto str = input.toStdString();
    tao::pegtl::parse<PEG::Expression, PEG::Action>(tao::pegtl::string_input(str, ""), ctx);
    auto ret = ctx.format();
    input    = QString::fromStdString(ctx.output);
    pos      = input.size();
    return ret ? QValidator::State::Intermediate : QValidator::State::Acceptable;
}

void HexValidator::fixup(QString& input) const
{
    PEG::Context ctx;
    tao::pegtl::parse<PEG::Expression, PEG::Action>(tao::pegtl::string_input(input.toStdString(), ""), ctx);
    ctx.fixup();
    input = QString::fromStdString(ctx.output);
}

HexLineEdit::HexLineEdit(QWidget* parent) { setValidator(new HexValidator(this)); }

HexLineEdit::~HexLineEdit() {}
