﻿/*********************************************************************************
**                                                                              **
**  Copyright (C) 2024-2025 LiLong                                              **
**  This file is part of OpenVisaApplication.                                   **
**                                                                              **
**  OpenVisaApplication is free software: you can redistribute it and/or modify **
**  it under the terms of the GNU General Public License as published by        **
**  the Free Software Foundation, either version 3 of the License, or           **
**  (at your option) any later version.                                         **
**                                                                              **
**  OpenVisaApplication is distributed in the hope that it will be useful,      **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of              **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               **
**  GNU General Public License for more details.                                **
**                                                                              **
**  You should have received a copy of the GNU General Public License           **
**  along with OpenVisaApplication. If not, see <https://www.gnu.org/licenses/>.**
**********************************************************************************/
#include "AttributeDialog.h"
#include "AttributeModel.h"
#include "BaudRateDelegate.h"
#include "DataBitsDelegate.h"
#include "FlowControlDelegate.h"
#include "ParityDelegate.h"
#include "StopBitsDelegate.h"
#include "TermCharsDelegate.h"
#include "TimeoutDelegate.h"

struct AttributeDialog::Impl
{
    AttributeModel* model;
};

AttributeDialog::AttributeDialog(std::shared_ptr<OpenVisa::Object> obj, bool isSerialPort, QWidget* parent /*= nullptr*/)
    : QCtmDialog(parent), m_impl(std::make_unique<Impl>())
{
    ui.setupUi(centralWidget());
    m_impl->model = new AttributeModel(this);
    m_impl->model->setObject(obj, isSerialPort);
    ui.tableView->setModel(m_impl->model);
    ui.tableView->setItemDelegateForRow(AttributeModel::TermChars, new TermCharsDelegate(ui.tableView));
    ui.tableView->setItemDelegateForRow(AttributeModel::Timeout, new TimeoutDelegate(ui.tableView));
    ui.tableView->setItemDelegateForRow(AttributeModel::BaudRate, new BaudRateDelegate(ui.tableView));
    ui.tableView->setItemDelegateForRow(AttributeModel::DataBits, new DataBitsDelegate(ui.tableView));
    ui.tableView->setItemDelegateForRow(AttributeModel::StopBits, new StopBitsDelegate(ui.tableView));
    ui.tableView->setItemDelegateForRow(AttributeModel::FlowControl, new FlowControlDelegate(ui.tableView));
    ui.tableView->setItemDelegateForRow(AttributeModel::Parity, new ParityDelegate(ui.tableView));

    connect(ui.okBtn, &QPushButton::clicked, this, &AttributeDialog::onOKBtn);
    connect(ui.cancelBtn, &QPushButton::clicked, this, &AttributeDialog::reject);
}

AttributeDialog::~AttributeDialog() {}

void AttributeDialog::onOKBtn()
{
    m_impl->model->apply();
    accept();
}
