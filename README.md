# OpenVisaApplication

基于 [OpenVisa](https://gitee.com/llongww/OpenVisa) 开发的应用程序，提供仪器通信操作。

可到转到发布页面，下载预编译的可执行程序 https://gitee.com/llongww/OpenVisaApplication/releases